# ARTIFACT REPOSITORY MANAGER WITH NEXUS 

## Table of Contents
- [Project Description](#project-description)

- [Installation](#installation)

- [Screenshots](#screenshots)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)
- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description
* Installed and configured Nexus from scratch on a cloud server(DigitalOcean)

* Created new User on Nexus with relevant permissions

* Java Gradle Project: Built Jar & Uploaded it to Nexus

* Java Maven Project: Built Jar & Uploaded it to Nexus 

## Installation

* $ sudo apt install openjdk-8-jre-headless

* $ sudo apt install net-tools 

* $ brew install maven 

* wget -c https://download.sonatype.com/nexus/3/nexus-3.42.0-01-unix.tar.gz

## Usage 

* /opt/nexus-3.42.0-01/bin/nexus start 

## Screenshots 

![Cloud Server](/images/01_Cloud_server.png)
> Displays Cloud server on DigitalOcean 

![Acces to Port 22](/images/02_acces_to_port22.png)
> Opened port 22 on DigitalOcean 

![Firewall configuration connected to server](/images/03_firewall_rule_connected_to_server.png)
> Firewall configuration connected to server 

![Connection to Server](/images/04_connecting_to_cloud_server.png)
> Connection to Server through ssh

![Java Installation](/images/05_Java_installed.png)
> Java Installation on server

![Nexus Dowload](/images/06_Nexus_Downloaded.png) 
> Command to Download Nexus 

![Extracting Nexus](/images/07_extracting_nexus.png)
> Command to Extract Nexus 

![Nexus User](/images/08_creating_nexus_user.png)
> Nexus User Creation 

![Changing Ownership](/images/09_changing_ownership_of_nexus_files.png)
> Changing Ownership on Cloud 

![Nexus Access Configuration](/images/10_changing_configuration_to_run_as_nexus_user.png)

![launching Nexus](/images/11_starting_nexus.png)
> Nexus Launch 

![Net-tools Installation](/images/12_installing_net_tools.png)
> Net-tools Installation Command 

![Nexus Activity](/images/13_checking_if_its_running.png)
> Checking Nexus Activiness 

![Port 8081](/images/14_opening_port_8081.png)
> Opening Port 8081 

![Nexus UI](/images/15_nexus_UI.png)

![Nexus Admin Password](/images/16_find_nexus_admin_password.png)
> Nexus Admin Password

![Creating Role](/images/17_creating_role.png)
> Creating Role on Nexus 

![Assigning Role](/images/18_assigning_role_to_user.png)
> Assigning Role on Nexus

![Uploaded Gradle Project](/images/19_java_gradle_project_published_to_nexus.png) 
> Uploaded Gradle Project

![Gradle Project](/images/20_gradle_application_nexus.png)
> Gradle Project on Nexus 

![Maven Credentials](/images/21_configuring_usercredentials.png)
> Maven User Credentials 

![Uploaded Maven Project](/images/22_moving_maven_app_to_nexus.png)
> Uploaded Maven Project 

![Maven Project](/images/23_maven_app_displayed_on_nexus.png)
> Maven Project on Nexus 

![User Repos](/images/24_viewing_avilable_repositories_for_a_user.png)
> User Available Repo 

![User Repo Display](images/25_user_available_repo)
> User Repo Available 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/cloud-and-infrastructure-as-service-basics.git

2. Create new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review 

## Tests

Test were run using:

- $ ./gradlew test 
- $ mvn test

## Questions

feel free to contact me for further questions via:

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com 

## References

https://gitlab.com/omacodes98/artifact-repository-manager-with-nexus

## License

The MIT License 

for more information you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji. 